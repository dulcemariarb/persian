﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Milibreria;

namespace persian
{
    public partial class pago : Formabase
    {
        public pago()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNumExpe.Text.Trim()) == false)
            {
                try
                {
                    string cmd = string.Format("Select Nom_pac From cliente where num_expediente='{0}'", txtNumExpe.Text.Trim());
                    DataSet ds = Utilidades.Ejecutar(cmd);
                    txtPaciente.Text = ds.Tables[0].Rows[0]["Nom_pac"].ToString().Trim();
                    txtCodigoPro.Focus();
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error: " + error.Message);
                }

            }
        }
        public static int contador_fila = 0;
        public static double total;
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnColocar_Click(object sender, EventArgs e)
        {
          
                bool existe = false;
                int num_fila = 0;
                if (contador_fila == 0)
                {
                    dataGridView1.Rows.Add(txtCodigoPro.Text, txtDescripcion.Text, txtPrecio.Text, txtCantidad.Text);
                    double importe = Convert.ToDouble(dataGridView1.Rows[contador_fila].Cells[2].Value) * Convert.ToDouble(dataGridView1.Rows[contador_fila].Cells[3].Value);
                    dataGridView1.Rows[contador_fila].Cells[4].Value = importe;
                    contador_fila++;
                }
                else
                {
                    foreach (DataGridViewRow Fila in dataGridView1.Rows)
                    {
                        if (Fila.Cells[0].Value.ToString() == txtCodigoPro.Text)
                        {
                            existe = true;
                            num_fila = Fila.Index;
                        }
                    }

                    if (existe == true)
                    {
                        dataGridView1.Rows[num_fila].Cells[3].Value = (Convert.ToDouble(txtCantidad.Text) + Convert.ToDouble(dataGridView1.Rows[num_fila].Cells[3].Value)).ToString();
                        double importe = Convert.ToDouble(dataGridView1.Rows[num_fila].Cells[2].Value) * Convert.ToDouble(dataGridView1.Rows[num_fila].Cells[3].Value);
                        dataGridView1.Rows[num_fila].Cells[4].Value = importe;
                    }
                    else
                    {
                        dataGridView1.Rows.Add(txtCodigoPro.Text, txtDescripcion.Text, txtPrecio.Text, txtCantidad.Text);
                        double importe = Convert.ToDouble(dataGridView1.Rows[contador_fila].Cells[2].Value) * Convert.ToDouble(dataGridView1.Rows[contador_fila].Cells[3].Value);
                        dataGridView1.Rows[contador_fila].Cells[4].Value = importe;
                        contador_fila++;
                    }
                }
                total = 0;
                foreach (DataGridViewRow Fila in dataGridView1.Rows)
                {
                    total += Convert.ToDouble(Fila.Cells[4].Value);
                }
                lblTotal.Text = "MX$" + total.ToString();
            }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            if (contador_fila > 0)
            {
                total = total - (Convert.ToDouble(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[4].Value));
                lblTotal.Text = "MX$" + total.ToString();
                dataGridView1.Rows.RemoveAt(dataGridView1.CurrentRow.Index);
                contador_fila--;
            }
        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            consultarPaciente conCli = new consultarPaciente();
            conCli.ShowDialog();
            if (conCli.DialogResult == DialogResult.OK)
            {
                txtNumExpe.Text = conCli.dataGridView1.Rows[conCli.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                txtPaciente.Text = conCli.dataGridView1.Rows[conCli.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();

                txtCodigoPro.Focus();
            }
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            ConsultarTratamientos conPro = new ConsultarTratamientos();
            conPro.ShowDialog();
            if (conPro.DialogResult == DialogResult.OK)
            {
                txtCodigoPro.Text = conPro.dataGridView1.Rows[conPro.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                txtDescripcion.Text = conPro.dataGridView1.Rows[conPro.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
                txtPrecio.Text = conPro.dataGridView1.Rows[conPro.dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();

                txtCantidad.Focus();
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Nuevo();
        }
        public override void Nuevo()
        {
            txtNumExpe.Text = "";
            txtPaciente.Text = "";
            txtCodigoPro.Text = "";
            txtDescripcion.Text = "";
            txtPrecio.Text = "";
            txtCantidad.Text = "";
            lblTotal.Text = "MX$ 0";
            dataGridView1.Rows.Clear();
            contador_fila = 0;
            total = 0;
            txtNumExpe.Focus();

        }

        private void btnFacturar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("La factura se ha envido a su correo");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Menu en = new Menu();
            en.Visible = true;
            this.Visible = false;
        }
    }
}

