﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Milibreria;

namespace persian
{
    public partial class ventanalogin : Formabase
    {
        public ventanalogin()
        {
            InitializeComponent();
        }

        private void ventanalogin_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string CMD = string.Format("SELECT id_usuario, Nom_usu, usuario, contrasena, status_medic FROM Usuarios WHERE(usuario = '{0}') AND (contrasena = '{1}')", txtNomAcc.Text.Trim(), txtPass.Text.Trim());

                DataSet ds = Utilidades.Ejecutar(CMD);

                string cuenta = ds.Tables[0].Rows[0]["usuario"].ToString().Trim();

                string contra = ds.Tables[0].Rows[0]["contrasena"].ToString().Trim();
                if (cuenta == txtNomAcc.Text.Trim() && contra == txtPass.Text.Trim())
                {
                    if (Convert.ToBoolean(ds.Tables[0].Rows[0]["status_medic"]) == true)
                    {
                        Menu me = new Menu();
                        this.Hide();
                        me.Show();
                    }
                    
                }
                   
            }
            catch (Exception error)
            {
                MessageBox.Show("Usuario o contraseña incorrecta " + error.Message);
            }
        }

        private void ventanalogin_FormClosed(object sender, FormClosedEventArgs e)
        {

            Application.Exit();

        }
    }
}

