﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Milibreria;

namespace persian
{
    public partial class HistoriaClinica : guardar
    {
        public HistoriaClinica()
        {
            InitializeComponent();
        }
        string strChecked = "";
        public override Boolean Guardar()
        {
            string cmd;
            try
            {
                cmd = string.Format("EXEC Historia {0},'{1}','{2}',{3},'{4}','{5}','{6}','{7}','{8}',{9},'{10}'",
                    txtExpediente.Text.Trim(), txtProblemasTratamientosPrevios.Text.Trim(), txtQueMedicamento.Text.Trim(),
                    txtAlergias.Text.Trim(), txtFrecuencia.Text.Trim(), txtMeses.Text.Trim(), txtComentarios.Text.Trim(), txtRecibeTratamiento.Text.Trim(), txtPadeceEnfermedad.Text.Trim(),
                    txtFuma.Text.Trim(), txtEmbrarazada.Text.Trim());
            Utilidades.Ejecutar(cmd);
                MessageBox.Show("se ha guardado correctamente");
                return true;
            }
            catch (Exception error)
            {
                MessageBox.Show("ha ocurrido un error " + error.Message);
                return false;
            }
        }


        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label28_Click(object sender, EventArgs e)
        {

        }

        private void txtExpediente_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Menu men = new Menu();
            men.Visible = true;
            this.Visible = false;
        }
    }
}
