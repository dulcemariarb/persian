﻿namespace persian
{
    partial class HistoriaClinica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtProblemasTratamientosPrevios = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtQueMedicamento = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtAlergias = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txtMeses = new System.Windows.Forms.TextBox();
            this.txtComentarios = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtFrecuencia = new System.Windows.Forms.TextBox();
            this.txtExpediente = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPadeceEnfermedad = new System.Windows.Forms.TextBox();
            this.txtFuma = new System.Windows.Forms.TextBox();
            this.txtEmbrarazada = new System.Windows.Forms.TextBox();
            this.txtRecibeTratamiento = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGua
            // 
            this.btnGua.Location = new System.Drawing.Point(50, 504);
            this.btnGua.Size = new System.Drawing.Size(187, 23);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(541, 504);
            this.btnSalir.Size = new System.Drawing.Size(187, 23);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(285, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Historia Clinica";
            // 
            // txtProblemasTratamientosPrevios
            // 
            this.txtProblemasTratamientosPrevios.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProblemasTratamientosPrevios.Location = new System.Drawing.Point(12, 124);
            this.txtProblemasTratamientosPrevios.Name = "txtProblemasTratamientosPrevios";
            this.txtProblemasTratamientosPrevios.Size = new System.Drawing.Size(732, 22);
            this.txtProblemasTratamientosPrevios.TabIndex = 39;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(8, 165);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(296, 16);
            this.label22.TabIndex = 40;
            this.label22.Text = "¿Recibe tratamiento médico actualmente? Si/No";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(5, 200);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(342, 16);
            this.label23.TabIndex = 41;
            this.label23.Text = "Si repondio si anteriormente ¿Que medicamentos toma?";
            // 
            // txtQueMedicamento
            // 
            this.txtQueMedicamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQueMedicamento.Location = new System.Drawing.Point(8, 225);
            this.txtQueMedicamento.Name = "txtQueMedicamento";
            this.txtQueMedicamento.Size = new System.Drawing.Size(732, 22);
            this.txtQueMedicamento.TabIndex = 43;
            this.txtQueMedicamento.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(8, 274);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(505, 16);
            this.label24.TabIndex = 44;
            this.label24.Text = "Algún miembro de su familia padece o padeció alguna enfermedad cronica ¿cual?: ";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(5, 312);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(437, 16);
            this.label29.TabIndex = 53;
            this.label29.Text = "¿Es alergico a algún medicamento, compuesto o alimento?¿A cuál (es)?";
            // 
            // txtAlergias
            // 
            this.txtAlergias.Location = new System.Drawing.Point(8, 343);
            this.txtAlergias.Name = "txtAlergias";
            this.txtAlergias.Size = new System.Drawing.Size(732, 20);
            this.txtAlergias.TabIndex = 56;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(8, 383);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(93, 16);
            this.label31.TabIndex = 57;
            this.label31.Text = "¿Fuma? Si/No";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(270, 382);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(133, 16);
            this.label32.TabIndex = 59;
            this.label32.Text = "¿Con qué fecuencia?";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(8, 420);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(235, 16);
            this.label33.TabIndex = 60;
            this.label33.Text = "Si es mujer ¿Está embarazada? Si/No";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(382, 422);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(115, 16);
            this.label34.TabIndex = 62;
            this.label34.Text = "¿Cuántos meses?";
            // 
            // txtMeses
            // 
            this.txtMeses.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMeses.Location = new System.Drawing.Point(503, 420);
            this.txtMeses.Name = "txtMeses";
            this.txtMeses.Size = new System.Drawing.Size(101, 22);
            this.txtMeses.TabIndex = 63;
            // 
            // txtComentarios
            // 
            this.txtComentarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComentarios.Location = new System.Drawing.Point(97, 464);
            this.txtComentarios.Name = "txtComentarios";
            this.txtComentarios.Size = new System.Drawing.Size(646, 22);
            this.txtComentarios.TabIndex = 65;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(4, 464);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(87, 16);
            this.label35.TabIndex = 64;
            this.label35.Text = "Comentarios:";
            // 
            // txtFrecuencia
            // 
            this.txtFrecuencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFrecuencia.Location = new System.Drawing.Point(419, 376);
            this.txtFrecuencia.Name = "txtFrecuencia";
            this.txtFrecuencia.Size = new System.Drawing.Size(136, 22);
            this.txtFrecuencia.TabIndex = 96;
            // 
            // txtExpediente
            // 
            this.txtExpediente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExpediente.Location = new System.Drawing.Point(640, 58);
            this.txtExpediente.Name = "txtExpediente";
            this.txtExpediente.Size = new System.Drawing.Size(100, 22);
            this.txtExpediente.TabIndex = 98;
            this.txtExpediente.TextChanged += new System.EventHandler(this.txtExpediente_TextChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(459, 58);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(148, 16);
            this.label36.TabIndex = 97;
            this.label36.Text = "Numero De Expediente";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(287, 504);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(187, 23);
            this.button1.TabIndex = 99;
            this.button1.Text = "Menu";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(9, 95);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(409, 16);
            this.label21.TabIndex = 20;
            this.label21.Text = "Problemas graves relacionados con tratamientos previos ¿Cuáles?";
            // 
            // txtPadeceEnfermedad
            // 
            this.txtPadeceEnfermedad.Location = new System.Drawing.Point(516, 270);
            this.txtPadeceEnfermedad.Name = "txtPadeceEnfermedad";
            this.txtPadeceEnfermedad.Size = new System.Drawing.Size(224, 20);
            this.txtPadeceEnfermedad.TabIndex = 100;
            // 
            // txtFuma
            // 
            this.txtFuma.Location = new System.Drawing.Point(107, 378);
            this.txtFuma.Name = "txtFuma";
            this.txtFuma.Size = new System.Drawing.Size(136, 20);
            this.txtFuma.TabIndex = 101;
            // 
            // txtEmbrarazada
            // 
            this.txtEmbrarazada.Location = new System.Drawing.Point(249, 420);
            this.txtEmbrarazada.Name = "txtEmbrarazada";
            this.txtEmbrarazada.Size = new System.Drawing.Size(100, 20);
            this.txtEmbrarazada.TabIndex = 102;
            // 
            // txtRecibeTratamiento
            // 
            this.txtRecibeTratamiento.Location = new System.Drawing.Point(323, 161);
            this.txtRecibeTratamiento.Name = "txtRecibeTratamiento";
            this.txtRecibeTratamiento.Size = new System.Drawing.Size(142, 20);
            this.txtRecibeTratamiento.TabIndex = 103;
            // 
            // HistoriaClinica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 539);
            this.Controls.Add(this.txtRecibeTratamiento);
            this.Controls.Add(this.txtEmbrarazada);
            this.Controls.Add(this.txtFuma);
            this.Controls.Add(this.txtPadeceEnfermedad);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtExpediente);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.txtFrecuencia);
            this.Controls.Add(this.txtComentarios);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.txtMeses);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.txtAlergias);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.txtQueMedicamento);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.txtProblemasTratamientosPrevios);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label1);
            this.Name = "HistoriaClinica";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HistoriaClinica";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label21, 0);
            this.Controls.SetChildIndex(this.txtProblemasTratamientosPrevios, 0);
            this.Controls.SetChildIndex(this.label22, 0);
            this.Controls.SetChildIndex(this.label23, 0);
            this.Controls.SetChildIndex(this.txtQueMedicamento, 0);
            this.Controls.SetChildIndex(this.label24, 0);
            this.Controls.SetChildIndex(this.label29, 0);
            this.Controls.SetChildIndex(this.txtAlergias, 0);
            this.Controls.SetChildIndex(this.label31, 0);
            this.Controls.SetChildIndex(this.label32, 0);
            this.Controls.SetChildIndex(this.label33, 0);
            this.Controls.SetChildIndex(this.label34, 0);
            this.Controls.SetChildIndex(this.txtMeses, 0);
            this.Controls.SetChildIndex(this.label35, 0);
            this.Controls.SetChildIndex(this.txtComentarios, 0);
            this.Controls.SetChildIndex(this.txtFrecuencia, 0);
            this.Controls.SetChildIndex(this.label36, 0);
            this.Controls.SetChildIndex(this.txtExpediente, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.btnGua, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.txtPadeceEnfermedad, 0);
            this.Controls.SetChildIndex(this.txtFuma, 0);
            this.Controls.SetChildIndex(this.txtEmbrarazada, 0);
            this.Controls.SetChildIndex(this.txtRecibeTratamiento, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtProblemasTratamientosPrevios;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtQueMedicamento;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtAlergias;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtMeses;
        private System.Windows.Forms.TextBox txtComentarios;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtFrecuencia;
        private System.Windows.Forms.TextBox txtExpediente;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtRecibeTratamiento;
        private System.Windows.Forms.TextBox txtEmbrarazada;
        private System.Windows.Forms.TextBox txtFuma;
        private System.Windows.Forms.TextBox txtPadeceEnfermedad;
        private System.Windows.Forms.Label label21;
    }
}