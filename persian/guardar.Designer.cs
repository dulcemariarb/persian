﻿namespace persian
{
    partial class guardar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGua = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(352, 262);
            // 
            // btnGua
            // 
            this.btnGua.Location = new System.Drawing.Point(61, 261);
            this.btnGua.Name = "btnGua";
            this.btnGua.Size = new System.Drawing.Size(75, 23);
            this.btnGua.TabIndex = 2;
            this.btnGua.Text = "Guardar";
            this.btnGua.UseVisualStyleBackColor = true;
            this.btnGua.Click += new System.EventHandler(this.btnGua_Click);
            // 
            // guardar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 333);
            this.Controls.Add(this.btnGua);
            this.Name = "guardar";
            this.Text = "guardar";
            this.Load += new System.EventHandler(this.guardar_Load);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.btnGua, 0);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button btnGua;
    }
}