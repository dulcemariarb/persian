﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Milibreria;

namespace persian
{
    public partial class consultarPaciente : Formabase
    {
        public consultarPaciente()
        {
            InitializeComponent();
        }

        public DataSet LLenarDataGV(string tabla)
        {
            DataSet DS;
            string cmd = string.Format("SELECT * FROM " + tabla);
            DS = Utilidades.Ejecutar(cmd);
            return DS;
        }

        private void consultarCliente_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = LLenarDataGV("DatosGenerales").Tables[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DatosGenerales datosGene = new DatosGenerales();
            datosGene.Visible = true;
            this.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text.Trim()) == false)
            {
                try
                {
                    DataSet ds;
                    string cmd = "SELECT * FROM DatosGenerales WHERE Nom_pac LIKE ('%" + textBox1.Text.Trim() + "%')";
                    string cm = "SELECT * FROM DatosGenerales WHERE num_expediente LIKE ('%" + textBox2.Text.Trim() + "%')";
                    ds = Utilidades.Ejecutar(cmd);
                    dataGridView1.DataSource = ds.Tables[0];
                }
                catch (Exception error)
                {
                    MessageBox.Show("Haocurrido un error: " + error.Message);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            menu.Visible = true;
            this.Visible = false;
        }

        private void Seleccionar_Click(object sender, EventArgs e)
        {

            if (dataGridView1.Rows.Count == 0)
            {
                return;
            }
            else
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
