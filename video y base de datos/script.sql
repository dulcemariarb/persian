USE [Persian]
GO
/****** Object:  Table [dbo].[Detalles]    Script Date: 04/12/2019 08:09:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Detalles](
	[CodPro] [int] NULL,
	[PrecioVen] [float] NULL,
	[CanVen] [float] NULL,
	[NumFac] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[NumFac] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DatosGenerales]    Script Date: 04/12/2019 08:09:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DatosGenerales](
	[Apellidos] [nchar](50) NULL,
	[FechaNac] [int] NULL,
	[Ocupacion] [nchar](50) NULL,
	[Calle] [nchar](50) NULL,
	[Num] [int] NULL,
	[Colonia] [nchar](50) NULL,
	[Ciudad] [nchar](50) NULL,
	[CoddigoPostal] [int] NULL,
	[MotivoConsulta] [nchar](1000) NULL,
	[Nom_pac] [nchar](50) NULL,
	[num_expediente] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[num_expediente] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 04/12/2019 08:09:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[Nom_usu] [nchar](50) NULL,
	[usuario] [nchar](50) NULL,
	[contrasena] [nchar](50) NULL,
	[status_medic] [bit] NULL,
	[id_usuario] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tratamiento]    Script Date: 04/12/2019 08:09:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tratamiento](
	[Nom_tratamiento] [nchar](10) NULL,
	[Precio] [float] NULL,
	[id_tratamiento] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_tratamiento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoriaClinica]    Script Date: 04/12/2019 08:09:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoriaClinica](
	[TratamientosPrevios] [nchar](100) NULL,
	[Medicamentos] [nchar](50) NULL,
	[CualesAlergias] [nchar](50) NULL,
	[FrecuenciaFuma] [nchar](50) NULL,
	[EmbarazadaMeses] [nchar](50) NULL,
	[Comentarios] [nchar](1000) NULL,
	[num_expediente] [int] NULL,
	[tratameinto_medico] [nchar](10) NULL,
	[famiiar_cronico] [nchar](50) NULL,
	[fuma] [nchar](10) NULL,
	[Embarazada] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Historia]    Script Date: 04/12/2019 08:09:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Historia]
@num_expediente int,@TratamientosPrevios nchar(100),
@Medicamentos nchar(50),@CualesAlergias nchar(50),@FrecuenciaFuma nchar(50),
@EmbarazadaMeses nchar(50),@Comentarios nchar(1000),@tratameinto_medico nchar(10),
@famiiar_cronico nchar(50),@fuma nchar(10),@Embarazada nchar(10)
as 

--DatosGenerales

if not exists (Select num_expediente FROM HistoriaClinica where num_expediente=@num_expediente)
insert into HistoriaClinica (num_expediente,TratamientosPrevios,Medicamentos,CualesAlergias,FrecuenciaFuma,
EmbarazadaMeses,Comentarios,tratameinto_medico,famiiar_cronico,fuma,Embarazada) 

values (@num_expediente,@TratamientosPrevios,@Medicamentos,@CualesAlergias,@FrecuenciaFuma,
@EmbarazadaMeses,@Comentarios,@tratameinto_medico,@famiiar_cronico,@fuma,@Embarazada)
else
update HistoriaClinica set @num_expediente=num_expediente,@TratamientosPrevios=TratamientosPrevios,@Medicamentos=Medicamentos,
@CualesAlergias=CualesAlergias,@FrecuenciaFuma=FrecuenciaFuma,@EmbarazadaMeses=EmbarazadaMeses,
@Comentarios=Comentarios,tratameinto_medico=@tratameinto_medico,famiiar_cronico=@famiiar_cronico,@fuma=fuma,@Embarazada=Embarazada
 where num_expediente=@num_expediente
GO
/****** Object:  StoredProcedure [dbo].[ActualizarTratamiento]    Script Date: 04/12/2019 08:09:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[ActualizarTratamiento]
@id_tratamiento int,@Nom_tratamiento nchar (100),@Precio float
as

--Actualiza articulos

if not exists (Select id_tratamiento FROM Tratamiento where id_tratamiento=@id_tratamiento)
insert into Tratamiento (Nom_tratamiento,Precio) values (@Nom_tratamiento,@precio)
else
update Tratamiento set Nom_tratamiento=@Nom_tratamiento,Precio=@Precio where id_tratamiento=@id_tratamiento
GO
/****** Object:  StoredProcedure [dbo].[Actualizar]    Script Date: 04/12/2019 08:09:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Actualizar]
@num_expediente int, @Nom_pac nchar(50),@Apellidos nchar(50),@FechaNac int,@Ocupacion nchar(50),@Calle nchar(50), 
@Num int,@Colonia nchar(50),@Ciudad nchar(50),@CoddigoPostal int,@MotivoConsulta nchar(1000)
as


if not exists (Select num_expediente FROM DatosGenerales where num_expediente=@num_expediente)
insert into DatosGenerales ( CoddigoPostal,Nom_pac,Apellidos,FechaNac,Ocupacion,Calle,Num,Colonia,Ciudad,MotivoConsulta) 
values (@CoddigoPostal,@Nom_pac,@Apellidos,@FechaNac,@Ocupacion,@Calle,@Num,@Colonia,@Ciudad,@MotivoConsulta)
else
update DatosGenerales set  Nom_pac=@Nom_pac,CoddigoPostal=@CoddigoPostal,Apellidos=@Apellidos,FechaNac=@FechaNac,Ocupacion=@Ocupacion,Calle=@Calle,
Num=@Num,Colonia=@Colonia,Ciudad=@Ciudad,MotivoConsulta=@MotivoConsulta where num_expediente=@num_expediente
GO
